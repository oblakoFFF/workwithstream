import java.io.InputStream;
import java.util.*;
import java.util.stream.Collectors;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

public class Main {
    private static final String FILE_NAME = "warandpeace.zip";

    public static void main(String[] args) throws Exception {
        //Инициализация
        int score = 0;
        List<String> words = new ArrayList<>();

        //Чтение
        ZipFile zipFile = new ZipFile(FILE_NAME);
        Enumeration<? extends ZipEntry> entries = zipFile.entries();
        while (entries.hasMoreElements()) {
            ZipEntry entry = entries.nextElement();
            try (InputStream in = zipFile.getInputStream(entry);
                 Scanner sc = new Scanner(in).useDelimiter("[^\\p{IsAlphabetic}]")) {
                while (sc.hasNext()) {
                    String token = sc.next();
                    String word = token.toLowerCase();
                    if (!word.isEmpty()) {
                        //Обработка
                        score++;
                        words.add(word);
                    }
                }
            }
        }

        //Результаты
        System.out.println("1. Количество слов: " + score);

        System.out.println("\n2. Количество различных слов: " + words.stream()
                .distinct()
                .count());

        System.out.println("\n3. TOP 100 слов по длинне:");
        words.stream()
                .sorted((o1, o2) -> o2.length() - o1.length())
                .limit(100)
                .collect(Collectors.toList())
                .forEach(System.out::println);

        System.out.println("\n4. TOP 100 слов по количеству повторений в файле:");
        //TODO

        System.out.println("\n5. Последние 100 слов в обратном порядке (начиная с последнего):");
        Collections.reverse(words);
        words.stream()
                .limit(100)
                .forEach(System.out::println);

        System.out.println("\n6. TOP 100 слов по алфавиту для каждой длинны (в порядке уменьшения длинны):");
        //TODO Формат вывода
        //1: а, б, в, ...
        //2: мы, ок, ...
        //3: она, они, ...
        //и так дал

        System.out.println("\n7. TOP 20 анаграмм (в порядке увеличения количества слов):");
        //TODO Формат вывода:
        // слова анараграммы 1 (наибольшее количество слов)
        // слова анаграммы 2
        // и так далее до 20-й анаграммы
    }
}